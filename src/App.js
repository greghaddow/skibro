import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Intro from './components/Intro';
import Schools from './components/Schools';
import Instructors from './components/Instructors';
import Experiences from './components/Experiences';
import Video from './components/Video';
import ResortDetail from './components/ResortDetail';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true
    }
  }
  componentDidMount() {
    fetch('https://test.skibro.com:3001/api/mobile/get-resortbyslug?slug=val-disere')
      .then(response => response.json())
      .then(data => this.setState({ data: data.data, isLoading: false }));
  }

  render() {

    if (!this.state.isLoading) {
    return (
      <div className="App">
        <Header pageTitle={this.state.data.name} />
        <main>
          <Intro title={this.state.data.oneLiner} text={this.state.data.text} />
          <Schools />
          <Instructors />
          <Experiences />
          <Video />
          <ResortDetail />
        </main>
        <Footer />
      </div>
    );
  }
   return (<div className="App">Loading...</div>)
  }
}

export default App;
