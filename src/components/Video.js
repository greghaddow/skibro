import React, { Component } from 'react';

export default class Video extends Component {
  render() {
    return (
      <section className="Video">
        <h2>Popcorn</h2>
        <h3>Val D'Isère in Video</h3>
        <video className="video"></video>
        <video className="video"></video>
        <video className="video"></video>
        <video className="video"></video>
        <video className="video"></video>
        <button className="previous">previous</button>
        <button className="next">next</button>
      </section>
    )
  }
}
