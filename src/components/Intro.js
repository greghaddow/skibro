import React, { Component } from 'react';

export default class Intro extends Component {
  render() {
    return (
      <section className="Intro">
        <div className="section__inner--bounding">
          <h2>{this.props.title}</h2>
          <p dangerouslySetInnerHTML={{__html:'<p>' + this.props.text.replace(/\n([ \t]*\n)+/g, '</p><p>')
                  .replace('\n', '<br />') + '</p>' }}></p>
        </div>
      </section>
    )
  }
}
