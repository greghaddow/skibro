import React, { Component } from 'react';
import './InstructorCard.css';
import StarRatings from 'react-star-ratings';
import { Button } from 'rmwc/Button';
export default class InstructorCard extends Component {
  render() {

    const cardStyle = {
      background: 'linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,0) 40%,rgba(255,255,255,1) 60%), url(' + this.props.backgroundImage + ') no-repeat top center',
      backgroundSize: '100%'
    };

    return (
      <article className="InstructorCard" style={cardStyle}>
        <div className="InstructorCard__content">
        <h3>{this.props.instructorName}</h3>
        <StarRatings
        name={this.props.instructorName}
        numberOfStars={5}
        rating={parseFloat(this.props.rating)}
        starRatedColor='#ef4035'
        emptyStarColor='#fff'

        starSpacing="2px"
        starDimension="14px"
        />
        <p>{this.props.content}</p>
        <Button dense  outlined theme="secondary" >Profile</Button>
        <Button dense unelevated >Book Me</Button>
        </div>

        <div className="InstructorCard__pricing">{this.props.pricing}</div>
      </article>
    )
  }
}
