import React, { Component } from 'react';
import './SchoolCard.css';

export default class SchoolCard extends Component {
  render() {

    const cardStyle = {
      backgroundImage: 'url(' + this.props.backgroundImage + ')',
      WebkitTransition: 'all', // note the capital 'W' here
      msTransition: 'all' // 'ms' is the only lowercase vendor prefix
    };

    return (
      <article className="SchoolCard" style={cardStyle}>
        <p className="SchoolCard__content">{this.props.content}</p>
        <div className="overlay">
          <h4>What<br />{this.props.schoolName} can offer you</h4>
          <button>See Our Services</button>
        </div>
      </article>
    )
  }
}
