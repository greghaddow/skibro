import React, { Component } from 'react';
import './Footer.css';

export default class Footer extends Component {
  render() {
    return (
      <header className="Footer">
        <div className="inner">
          <h3>Why Use Skibro?</h3>
            
          <div className="download">
            <h3>Download the Skibro App</h3>
          </div>
        </div>
      </header>
    )
  }
}
