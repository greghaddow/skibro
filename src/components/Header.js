import React, { Component } from 'react';
import './Header.css';
import headerImage from '../resources/images/header-skier.png';
import { Grid, GridCell } from 'rmwc/Grid';
import {
  Toolbar,
  ToolbarRow,
  ToolbarSection,
  ToolbarTitle,
  ToolbarMenuIcon,
  ToolbarIcon
} from 'rmwc/Toolbar';


export default class Header extends Component {
  render() {
    return (
      <header className="Header">
      <Toolbar style={{ backgroundColor: "transparent"}}>
        <ToolbarRow>
          <ToolbarTitle>Toolbar</ToolbarTitle>
          <ToolbarSection alignEnd>
            <ToolbarMenuIcon icon="menu"/>
          </ToolbarSection>
        </ToolbarRow>
      </Toolbar>
      <div className="section__inner--bounding">
      
      <Grid>

        <GridCell span="6">
        <img src={headerImage} alt="Val D'Isere" />
        </GridCell>
        <GridCell span="6">
          <h1>{this.props.pageTitle}</h1>
            <p>Winter 2018/2019: 
  Val d’Isère ski area in the French Alps will be open from Nov 24th until May 1st 2019</p>

        </GridCell>
        </Grid>
        </div>
        <div className="bookingStrip">BOOK YOUR PERFECT mountain EXPERIENCE</div>
      </header>
    )
  }
}
