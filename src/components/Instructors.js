import React, { Component } from 'react';
import InstructorCard from './InstructorCard';
import './Instructors.css';

export default class Instructors extends Component {
  render() {
    return (
      <section className="Instructors">
      <div className="section__inner--bounding">
        <h2>Private Instructors &amp; Mountain Guides</h2>
          <div className="grid-container">
        <InstructorCard backgroundImage="/images/nicolas.png" instructorName="Nicolas B" content="Passionate skier and nature addict" rating="4.3" pricing="&euro;70 / hr"/>
        <InstructorCard backgroundImage="/images/nicolas.png" instructorName="Corrine M" content="Passionate skier and nature addict" rating="5" pricing="&euro;70 / hr"/>
        <InstructorCard backgroundImage="/images/nicolas.png" instructorName="Anthony S" content="Passionate skier and nature addict" rating="4.1" pricing="&euro;70 / hr"/>
        <InstructorCard backgroundImage="/images/nicolas.png" instructorName="Gregory B" content="Passionate skier and nature addict" rating="4.7" pricing="&euro;70 / hr"/>
        <InstructorCard backgroundImage="/images/nicolas.png" instructorName="Jay G" content="Passionate skier and nature addict" rating="4.7" pricing="&euro;70 / half-day"/>
          </div>
        <button>See all our instructors</button>
        </div>
      </section>
    )
  }
}
