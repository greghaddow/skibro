import React, { Component } from 'react';
import './ResortDetail.css';

export default class ResortDetail extends Component {
  render() {
    return (
      <section className="ResortDetail">
        <div className="detail">
          <h2>Val D'Isère in Detail</h2>
          <h3>Vital Statistics</h3>
          <ul>
            <li>Altitude Top</li>
          </ul>
          <h3>About Val D'Isère</h3>
          <p>Lorem ipsum...</p>
          <div className="pisteMap"></div>
          <div className="snowForecast"></div>
          <div className="webCam"></div>
        </div>
        <div className="map">
        </div>

        <div className="amigos">
        </div>
      </section>
    )
  }
}
