import React, { Component } from 'react';
import SchoolCard from './SchoolCard';
import './Schools.css';

export default class Schools extends Component {
  render() {
    return (
      <section className="Schools">
        <div className="section__inner--bounding">
          <h2>Top Ski Schools in Val D'Isère</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed turpis ante, semper vitae volutpat vel, dignissim eget felis. Nullam eget gravida nulla. Fusce non placerat quam. Donec rutrum dolor tortor, et posuere elit hendrerit rutrum. In volutpat turpis in leo pretium, et vestibulum nisl dictum. In erat felis, tempus eleifend dignissim nec, rhoncus nec nibh.
</p>
          <div className="grid-container">
            <SchoolCard backgroundImage="/images/esf.png" schoolName="ESF" content="Our ​​350 Val D'isère ESF instructors invite you to discover a world of fun and freedom this winter." />
            <SchoolCard backgroundImage="/images/tdc.png" schoolName="TDC" content="TDC provides quality ski coaching clinics for people who really want to improve their skiing." />
            <SchoolCard backgroundImage="/images/progression.png" schoolName="Progression" content="Progression  is regarded as one of the leading ski &amp; snowboard schools in the Alps." />
            <SchoolCard backgroundImage="/images/oxygene.png" schoolName="Oxygene" content="The leading family  friendly, English-speaking ski &amp; snowboard school in the French Alps." />
          </div>
        </div>
      </section>
    )
  }
}
